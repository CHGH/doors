package com.chgh.thedoors.game;

import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class Game {

    private static final String TAG = "Game";

    private int mDoorWithCar;
    private int mDoorsAmount;
    private int mWonGames;
    private int mLostGames;
    private int mChosenDoor;
    private int mGamesInTotal;
    private boolean mFirstGuess;

    public Game(int doorsAmount) {
        mDoorsAmount = doorsAmount;
        newRound();
    }

    public void newRound() {
        mDoorWithCar = getRandomInteger(3);
        Log.i(TAG, "Door with a car: " + mDoorWithCar);
        mFirstGuess = false;
    }

    public void reset(int doorsAmount) {
        newRound();
        mDoorsAmount = doorsAmount;
        mWonGames = 0;
        mLostGames = 0;
        mGamesInTotal = 0;
    }

    public int getDoorWithoutCar() {
        int doorWithoutCar = 0;
        while (doorWithoutCar == mChosenDoor || doorWithoutCar == mDoorWithCar) {
            doorWithoutCar = getRandomInteger(3);
        }
        Log.i(TAG, "Door without car: " + doorWithoutCar);
        return doorWithoutCar;
    }

    private int getRandomInteger(int n) {
        return new Random().nextInt(n);
    }

    public int getChosenDoor() {
        return mChosenDoor;
    }

    public void setChosenDoor(int chosenDoor) {
        mChosenDoor = chosenDoor;
    }

    public boolean isFirstGuess() {
        return mFirstGuess;
    }

    public void setFirstGuess(boolean firstGuess) {
        mFirstGuess = firstGuess;
    }

    public int getDoorWithCar() {
        return mDoorWithCar;
    }

    public int getWonGames() {
        return mWonGames;
    }

    public void setWonGames(int wonGames) {
        mWonGames = wonGames;
    }

    public int getLostGames() {
        return mLostGames;
    }

    public void setLostGames(int lostGames) {
        mLostGames = lostGames;
    }

    public int getGamesInTotal() {
        return mGamesInTotal;
    }

    public void setGamesInTotal(int gamesInTotal) {
        mGamesInTotal = gamesInTotal;
    }

    public double getLostGamesPercent() {
        return mGamesInTotal == 0 ? 0
                : round((double) mLostGames / mGamesInTotal * 100, 2);
    }

    public double getWonGamesPercent() {
        return mGamesInTotal == 0 ? 0
                : round((double) mWonGames / mGamesInTotal * 100, 2);
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}

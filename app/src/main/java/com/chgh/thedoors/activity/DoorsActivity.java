package com.chgh.thedoors.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chgh.thedoors.R;
import com.chgh.thedoors.game.Game;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoorsActivity extends AppCompatActivity {

    @BindView(R.id.info_tv) TextView mInfoTV;
    @BindView(R.id.games_in_total_tv) TextView mGamesInTotalTV;
    @BindView(R.id.won_games_tv) TextView mWonGamesTV;
    @BindView(R.id.lost_games_tv) TextView mLostGamesTV;
    @BindView(R.id.new_round_b) Button mNewRoundB;
    @BindView(R.id.new_game_b) Button mNewGameB;
    @BindView(R.id.auto_b) Button mAutoB;
    private List<Button> mDoors;

    private Game mGame;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mDoors = new ArrayList<Button>() {{
            add(findViewById(R.id.door_1));
            add(findViewById(R.id.door_2));
            add(findViewById(R.id.door_3));
        }};

        mDoors.forEach(button -> button.setOnClickListener(mDoorListener));

        mNewRoundB.setOnClickListener(mNewRoundListener);
        mNewGameB.setOnClickListener(mNewGameListener);
        mAutoB.setOnClickListener(mAutoListener);

        mGamesInTotalTV.setText("0");
        mWonGamesTV.setText("0 (0%)");
        mLostGamesTV.setText("0 (0%)");
        mInfoTV.setText(R.string.chose_the_door);

        mGame = new Game(3);
    }

    private View.OnClickListener mDoorListener = view -> {
        Button door = (Button) view;
        int chosenDoor = Integer.parseInt(door.getTag().toString());

        if (!mGame.isFirstGuess()) {
            door.setText("?");
            mInfoTV.setText(R.string.are_you_sure);

            mGame.setFirstGuess(true);
            mGame.setChosenDoor(chosenDoor);

            int doorWithoutCar = mGame.getDoorWithoutCar();
            mDoors.get(doorWithoutCar).setText("X");
            mDoors.get(doorWithoutCar).setEnabled(false);

        } else {
            if (chosenDoor == mGame.getDoorWithCar()) {
                mInfoTV.setText(R.string.you_won);
                mDoors.get(chosenDoor).setText("V");
                mGame.setWonGames(mGame.getWonGames() + 1);
            } else {
                mInfoTV.setText(R.string.you_lost);
                mDoors.get(chosenDoor).setText("X");
                mGame.setLostGames(mGame.getLostGames() + 1);
            }
            mGame.setGamesInTotal(mGame.getGamesInTotal() + 1);
            for (int i = 0; i < mDoors.size(); i++) {
                mDoors.get(i).setEnabled(false);
            }
            updateStatisticsView();
        }
    };

    private View.OnClickListener mNewRoundListener = view -> {
        newRound();
    };

    private View.OnClickListener mNewGameListener = view -> {
        newGame();
    };

    private void updateStatisticsView() {
        mGamesInTotalTV.setText(String.valueOf(mGame.getGamesInTotal()));
        StringBuilder sb = new StringBuilder();
        sb.append(mGame.getWonGames()).append(" (").append(mGame.getWonGamesPercent()).append("%)");
        mWonGamesTV.setText(sb.toString());
        sb = new StringBuilder();
        sb.append(mGame.getLostGames()).append(" (").append(mGame.getLostGamesPercent()).append("%)");
        mLostGamesTV.setText(sb.toString());
    }

    private View.OnClickListener mAutoListener = view -> {

    };

    private void newRound() {
        mInfoTV.setText(R.string.chose_the_door);
        mGame.newRound();
        for (int i = 0; i < mDoors.size(); i++) {
            mDoors.get(i).setText(String.valueOf(i + 1));
            mDoors.get(i).setEnabled(true);
        }
    }

    private void newGame() {
        newRound();
        mGame.reset(3);
        updateStatisticsView();
    }
}
